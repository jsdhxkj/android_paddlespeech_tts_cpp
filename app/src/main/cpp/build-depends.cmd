@echo off
setlocal

REM Exit immediately if a command exits with a non-zero status.
set "ERRORLEVEL=0"

REM Print the directory of the script and change to the third-party directory
for %%i in ("%~dp0\third-party") do set "third_party_dir=%%~fi"
cd /d "%third_party_dir%"

echo Creating build directory...
mkdir build 2>nul
cd /d build

echo Running CMake...
cmake .. -G "Unix Makefiles"

REM Build the project using NMake if no arguments are passed, otherwise use the passed arguments
if "%1"=="" (
    echo Building with all available cores...
    make
) else (
    echo Building with passed arguments: %*
    make %*
)

echo Done.
endlocal