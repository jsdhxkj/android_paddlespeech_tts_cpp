@echo off
setlocal

REM Exit immediately if a command exits with a non-zero status.
set "ERRORLEVEL=0"

REM Get the directory of the script and change to that directory
for %%i in ("%~dp0") do set "script_dir=%%~fi"
cd /d "%script_dir%"

REM Remove the "build" directory if it exists
if exist .\build (
    rmdir /s /q .\build
)

REM Remove the "third-party\build" directory if it exists
if exist .\third-party\build (
    rmdir /s /q .\third-party\build
)

echo Done.
endlocal