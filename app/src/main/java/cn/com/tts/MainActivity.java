package cn.com.tts;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;

import cn.com.tts.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'tts' library on application startup.
    static {
        System.loadLibrary("paddlespeech_tts_front");
    }

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        float[] floats = getPhoneIdsJNI("你好");

        TextView tv = binding.sampleText;
        tv.setText(Arrays.toString(floats));
    }

    // 声明本地方法
    public static native float[] getPhoneIdsJNI(String sentence);

}