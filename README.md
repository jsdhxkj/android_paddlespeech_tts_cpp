# android_paddlespeech_tts_cpp

#### 介绍
基于paddlespeech在android部署的tts文本前端，做一个andorid端文本前端编译
暂时还无法编译通过

原仓库：https://github.com/lym0302/paddlespeech_tts_cpp

#### 软件架构
软件架构说明


#### 安装教程
linux下编译
1. 下载Android Sdk，配置环境变量，执行 `sdkmanager --license`
2. 设置JDK_HOME，需要使用jdk17版本
3. 配置local.properties，设置sdk.dir
4. 安装编译环境 ubuntu `sudo apt install build-essential cmake pkg-config` centos `sudo yum groupinstall "Development Tools"` `sudo yum install cmake`
5. 先执行 `./gradlew build`
6. 执行 `./app/src/main/cpp/build.sh`
7. 执行 `./gradlew assembleRelease`
8. 补充说明：下载android sdk后，将文件放置以下路径
--- home
----- Android
------- Sdk
--------- cmdline-tools
----------- latest
------------- 解压后文件夹内的文件

win下编译
安装编译环境
1. 从 `https://github.com/niXman/mingw-builds-binaries/releases` 下载 已编译好的mingw包，根据自己系统选择对应的版本,win下选择`x86_64-13.2.0-release-win32-seh-msvcrt-rt_v11-rev1.7z`
2. 解压到任意位置，将解压后的文件夹放到 `C:\MinGW`
3. 配置环境变量，将 `C:\MinGW\bin` 添加到环境变量中
4. 输入`gcc -v`检查环境变量是否配置正常
5. 将`C:\MinGW\bin\mingw32-make.exe` 复制一份，命名为 `make.exe`
6. 安装CMake，下载地址`https://cmake.org/`
7. 导入项目到Android Studio中，编译

安装 [msys2-x86_64](https://www.msys2.org/)
```shell
pacman -Syuu
pacman -S mingw-w64-x86_64-gcc
pacman -S mingw-w64-x86_64-pkg-config
pacman -S mingw-w64-x86_64-zlib
pacman -S mingw-w64-x86_64-make
```

如果git clone项目失败的话，可以尝试以下命令
```shell
git config --global --unset http.proxy
git config --global --unset https.proxy

#配置本地代理
git config --global http.proxy 127.0.0.1:7890
git config --global https.proxy 127.0.0.1:7890
```
gflags 项目要求camake版本为3.0.2，所以fork项目，修改cmake版本为3.5.0
编译前需要先执行 `build-depends.com` 文件

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
